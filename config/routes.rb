BitcoinAutoSellBroker::Application.routes.draw do

  get "coinbase/info"
  namespace :coinbase do
    resources :requests
  end

  resources :blockchain, only: :index

  resources :transactions do
    resource :deposits, only: [:new]
  end

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  devise_scope :user do
    get 'sign_in', :to => 'users#signin', :as => :new_user_session
    get 'sign_in', :to => 'users#signin', :as => :new_session
    get 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_session
  end

  root 'transactions#index'

end
