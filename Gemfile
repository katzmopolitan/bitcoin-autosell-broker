source 'https://rubygems.org'

ruby '2.1.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.2'
gem 'simple_form'
gem 'thin'
gem 'sidekiq'

gem 'pg'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

gem 'figaro'

gem 'coinbase', '~> 2.0', github: "coinbase/coinbase-ruby"
gem 'bitstamp'
gem 'rest-client'
gem 'rqrcode'
gem 'chain-ruby', require: 'chain'
gem 'foundation-rails'

gem 'omniauth-coinbase', '~> 1.0'

gem 'devise'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

gem 'rails-rename'

group :development, :test do
  gem 'pry-byebug'
  gem "pry-remote"
  gem "pry-rails"
  gem "foreman"
  gem "proxylocal"
  gem 'quiet_assets'

  gem 'factory_girl_rails'
  gem 'rspec-rails', '~> 2.0'
  gem 'database_cleaner'
  gem 'launchy'
end

gem 'rails_12factor', group: :production
