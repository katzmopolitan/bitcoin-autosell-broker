require 'spec_helper'

describe BlockchainController do
  let(:trx) { trx = create(:transaction, incoming_address: "1WGRTYGF", amount: 1) }

  it "should update the transaction for successful validation" do
    value = trx.amount * 100000000

    expect(BestPriceJob).to receive(:perform)

    get :index,
      anonymous: false,
      value: value,
      input_address: trx.incoming_address,
      destination_address: "something",
      transaction_hash: "6403c1d33a027338ed0a553817c2077ec383876893b65217f427275db30b2d5f",
      input_transaction_hash: "124fb77c10dca5b247202835c13b7cfbf0417ce08e9333d61128d827223b46f4"

    trx.reload.status.should eq Transaction::AMOUNT_RECIEVED
  end

  it "should start selling BTC" do
    value = trx.amount * 100000000

    expect(BestPriceJob).to receive(:perform).with(trx.id)

    get :index,
      anonymous: false,
      value: value,
      input_address: trx.incoming_address,
      destination_address: "something"

  end

  it "should update the transaction as wrong amount" do
    value = trx.amount * 100000000 + 1

    get :index,
      value: value,
      input_address: trx.incoming_address,
      destination_address: "something"

    trx.reload.status.should eq Transaction::WRONG_AMOUNT_RECIEVED
  end

end
