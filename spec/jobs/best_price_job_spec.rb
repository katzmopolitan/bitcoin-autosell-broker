require 'spec_helper'

describe BestPriceJob do

  it "should sell with coinbase if price is lower" do
    tx = create(:transaction)
    coinbase = CoinbaseService.new
    bitstamp = BitstampService.new

    expect(BestPriceJob).to receive(:coinbase_service).twice.and_return(coinbase)
    expect(BestPriceJob).to receive(:bitstamp_service).and_return(bitstamp)

    lowest_price = Money.new(100)
    expect(coinbase).to receive(:price).and_return(lowest_price)
    expect(bitstamp).to receive(:price).and_return(Money.new(10000))

    expect(coinbase).to receive(:sell!).with(tx.amount).and_return(lowest_price * tx.amount)

    BestPriceJob.perform(tx.id)

    tx.reload.status.should eq Transaction::COMPLETED
    tx.usd_amount.should eq lowest_price * tx.amount
  end

  it "should sell with bitstamp if price is lower" do
    tx = create(:transaction)
    coinbase = CoinbaseService.new
    bitstamp = BitstampService.new

    expect(BestPriceJob).to receive(:coinbase_service).and_return(coinbase)
    expect(BestPriceJob).to receive(:bitstamp_service).twice.and_return(bitstamp)

    lowest_price = Money.new(1000)
    expect(coinbase).to receive(:price).and_return(Money.new(10000))
    expect(bitstamp).to receive(:price).and_return(lowest_price)

    expect(bitstamp).to receive(:sell!).with(tx.amount).and_return(lowest_price * tx.amount)

    BestPriceJob.perform(tx.id)

    tx.reload.status.should eq Transaction::COMPLETED
    tx.reload.usd_amount.should eq lowest_price * tx.amount
  end

end
