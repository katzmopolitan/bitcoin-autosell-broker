class AddUsdMountToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :usd_amount, :float
  end
end
