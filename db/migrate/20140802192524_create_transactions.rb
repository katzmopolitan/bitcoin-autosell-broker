class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.float :amount
      t.string :address
      t.string :status
      t.integer :user_id

      t.timestamps
    end
  end
end
