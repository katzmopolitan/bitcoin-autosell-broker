class AddIncomingAddressToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :incoming_address, :string
  end
end
