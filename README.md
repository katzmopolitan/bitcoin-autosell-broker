# BitcoinAutoSellBroker

## Setup

* Register two applications in (Coinbase](https://coinbase.com/oauth/applications)
  * http://127.0.0.1:3000/users/auth/coinbase/callback
  1. For broker server (BTCBrokerServer)
  1. For user authentication (BTCBroker)

* bundle install
* cp config/application.yml.example config/application.yml

* createuser -s broker
* rake db:create db:migrate
* PORT=3000 foreman start

## In development

* `proxylocal 3000 --host btc-broker` (same name as in application.yml)

## Tests

* rake db:migrate RAILS_ENV=test
* rspec

## How it works

Note: For simplicity, authentication to the system is done via Coinbase OAuth

**[Sell Screen](http://monosnap.com/image/BJLTwlb5nL6BBaVKfPI9fuWOTYP7mf.png)**

User enters amount to sell

**[Payment](http://monosnap.com/image/tXh7ZN0tJDHVBY8WWlQXTpFmQTXsoJ.png)**

* A new Bitcoin address is created for each transaction that user initiates using blockchain.info
* When Blockchain receives a payment to that new address, a callback is sent back to our server for validation

**Sell off**

* If validation of payment is successful, a job is started to seel BTC
   * app/job folder contains the logic responsible for selling BTC

* app/services folder contains the various communication to exchanges with the purpuse of pricing and selling BTC
   * coinbase service is implemented
   * not enough time to implement bitstamp, but it is not difficult as it requires only 2 methods

**[Current Transactions](http://monosnap.com/image/LZmzhaZde0rVbipG4NFTkFzAPia4NR.png)**
