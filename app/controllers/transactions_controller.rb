#Transactions that users initiate and can see status of
class TransactionsController < ApplicationController
  before_action :authenticate_user!

  def new
    @transaction = current_user.transactions.build
  end

  def create
    @transaction = current_user.transactions.build
    @transaction.update_attributes(transaction_params)
    redirect_to new_transaction_deposits_path(@transaction)
  end

  def index
    @transactions = current_user.transactions
  end

  private

  def transaction_params
    params.require(:transaction).permit(:amount)
  end

end
