#Deposits made by the user into our account so we can sell for them
class DepositsController < ApplicationController
  before_action :authenticate_user!

  def new
    @transaction = current_user.transactions.find(params[:transaction_id])

    #TODO: better to do with a state machine
    if @transaction.status != Transaction::WAITING_FOR_PAYMENT

      #get a new address that will forward all incoming payment to our address
      response = RestClient.get 'https://blockchain.info/api/receive', {
        params: {
          method: "create",
          address: Figaro.env.BROKER_PAYMENT_BITCOIN_ADDRESS,
          callback: "http://#{Figaro.env.HOST}/blockchain"
        }
      }
      Rails.logger.info response

      json = JSON.parse(response)
      @broker_wallet = json["input_address"]
      @transaction.incoming_address = @broker_wallet
      @transaction.status = Transaction::WAITING_FOR_PAYMENT
      @transaction.save
    else
      @broker_wallet = @transaction.incoming_address
    end

    @qr = RQRCode::QRCode.new(@broker_wallet)
  end
end
