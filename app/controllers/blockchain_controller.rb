#For validating that users made a payment
class BlockchainController < ApplicationController
  def index
    value = params[:value].to_f
    value = value/100000000 #satoshi!
    input_address= params[:input_address]
    destination_address = params[:destination_address]

    Rails.logger.info "Value: #{value}BTC"
    Rails.logger.info "Input address: #{input_address}"
    Rails.logger.info "Destination address: #{destination_address}"

    transaction = Transaction.find_by(incoming_address: input_address)
    if transaction.amount == value
      transaction.status = Transaction::AMOUNT_RECIEVED
    else
      transaction.status = Transaction::WRONG_AMOUNT_RECIEVED
    end
    transaction.save

    if transaction.ready_to_sell?
      #as soon as the user payment is received, attempt to sell it
      #TODO: create an asynchronous job
      BestPriceJob.perform(transaction.id)
    end

    #blockchain need this response
    render json: "*ok*"
  end
end
