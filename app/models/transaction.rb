class Transaction < ActiveRecord::Base
  #TODO: values are store as floats, it is better to keep them as integers and use Money gem
  WAITING_FOR_PAYMENT="waiting_payment".freeze
  AMOUNT_RECIEVED = "payment_receieved".freeze
  WRONG_AMOUNT_RECIEVED = "wrong_payment_received".freeze
  COMPLETED = "completed"

  def success?
    status == COMPLETED
  end

  def ready_to_sell?
    status == AMOUNT_RECIEVED
  end
end
