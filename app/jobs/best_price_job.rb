class BestPriceJob

  def self.perform(transaction_id)
    tx = Transaction.find(transaction_id)

    #check prices at coinbase
    coinbase_price = coinbase_service.price(tx.amount)

    #check prices at bitstamp
    bitstamp_price = bitstamp_service.price(tx.amount)

    usd = if coinbase_price < bitstamp_price
            coinbase_service.sell!(tx.amount)
          else
            bitstamp_service.sell!(tx.amount)
          end

    tx.status = Transaction::COMPLETED
    tx.usd_amount = usd.to_f
    tx.save
  end

  def self.coinbase_service
    CoinbaseService.new
  end

  def self.bitstamp_service
    BitstampService.new
  end

end
