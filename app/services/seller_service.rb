class SellerService

  def price(btc_amount)
    raise "implement"
  end

  # @params
  #   :btc_amount = amount in BTC to sell
  # @return
  #   amount in USD
  def sell!(btc_amount)
    raise "implement"
  end
end
