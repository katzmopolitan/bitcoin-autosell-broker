class CoinbaseService

  def price(btc_amount)
    coinbase.sell_price(btc_amount)
  end

  #sell BTC and returns value in USD
  def sell!(btc_amount)
    #keep your BTC, it is useful!
    if Figaro.env.PERFORM_COINBASE_SELL
      r = coinbase.sell!(btc_amount)
      r.transfer.total
    else
      btc_amount * price
    end
  end

  private

  def coinbase
    coinbase = Coinbase::Client.new(
      Figaro.env.COINBASE_BTC_SERVER_CLIENT_ID,
      Figaro.env.COINBASE_BTC_SERVER_CLIENT_SECRET)
  end

end
