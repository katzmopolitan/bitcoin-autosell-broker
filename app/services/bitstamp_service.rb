class BitstampService

  def price(btc_amount)
    Bitstamp::Ticker.last
  end

  #TODO: not enough time to test
  def sell!(btc_amount)
    Bitstamp.orders.buy(amount: btc_amount, price: Bitstamp::Ticker.last)
  end

end
